import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import AppRouter from "./AppRouter";
import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(

<Router>
<AppRouter />
</Router>,

document.getElementById("root"));
registerServiceWorker();
