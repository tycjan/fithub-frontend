import React, { Component } from "react";
import SignIn from "./components/SignIn";
import App from "./components/App";
import SignUp from "./components/SignUp";
import { Route } from "react-router-dom";

export default class AppRouter extends Component {
  render() {
    return (
      <div>
      <Route exact path ="/" component={SignIn} />
      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/app" component={App} />
    </div>
    );
  }
}
