import React, { Component } from "react";
import { FormGroup, Form, Input, Label, Button, Alert, Container, Col, Row } from "reactstrap";
import { Route, Link } from "react-router-dom";
import Jumbo from "./Jumbo";
import axios from "axios";

export default class SignIn extends Component {

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            token: "",
            loginAlert: false
        };
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.login = this.login.bind(this);
    }
    handleSubmit(e) {
        e.preventDefault();
        this.login();
    }

    login() {
        axios.post('https://fithubserver.herokuapp.com/api/user/login', {
            email: this.state.email,
            password: this.state.password
        }).then(response => {
            this.setState({ token: response.data.token });
        }).catch(err => {
            this.setState({loginAlert: true});
        });
    }

    handleEmailChange(event) {
        this.setState({ email: event.target.value });
    }

    handlePasswordChange(event) {
        this.setState({ password: event.target.value });
    }


    render() {
        let loginAlert = null;
        if (this.state.loginAlert) {
            loginAlert = <Alert color="danger">
                        <strong>Oh snap!</strong> Email or password are wrong :(
                    </Alert>;
        }
        return (
            <Container>
                <Row>
                    <Col>
                        {loginAlert}
                        <Jumbo/>
                        <Form>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input type="email" value={this.state.email} onChange={this.handleEmailChange} placeholder="email@example.com" />
                            </FormGroup>
                            <FormGroup>
                                <Label>Password</Label>
                                <Input type="password" value={this.state.password} onChange={this.handlePasswordChange} placeholder="some password" />
                            </FormGroup>
                            <Button outline color="primary" onClick={this.handleSubmit} block> Sign In </Button>
                            <br />
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }
}
