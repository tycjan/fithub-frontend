import React, { Component } from "react";
import { Jumbotron, Button } from "reactstrap";
import {Link} from "react-router-dom";

export default class Jumbo extends Component{
 
    render() {
        return (
            <Jumbotron>
            <h1 className="display-2">Fitness Hub.</h1>
            <p className="lead">Your new fit life starts here.</p>
            <hr className="my-2" />
            <p>Explore of posibility and simpilicity of using a fitness hub as diet and workout tracker.</p>
            <p className="lead">
            <Link to="/signup">
              <Button color="success">Sign Up</Button>
            </Link>
            </p>
          </Jumbotron>
        );
    }
}
