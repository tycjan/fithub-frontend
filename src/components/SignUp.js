import React, { Component } from "react";
import { FormGroup, Form, Input, Label, Button, Container, Col, Row, Alert } from "reactstrap";
import Jumbo from "./Jumbo";
import axios from "axios";

export default class SignUp extends Component {
    constructor() {
        super();
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            token: "",
            registerAlert: false
        };
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.register = this.register.bind(this);
        this.login = this.login.bind(this);
    }

    handleEmailChange(event) {
        this.setState({ email: event.target.value });
    }

    handlePasswordChange(event) {
        this.setState({ password: event.target.value });
    }

    handleFirstNameChange(event) {
        this.setState({ firstName: event.target.value });
    }

    handleLastNameChange(event) {
        this.setState({ lastName: event.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.register();
    }

    register() {
        axios.post('https://fithubserver.herokuapp.com/api/user', {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            password: this.state.password
        }).then(response => {
            this.login();
        }).catch(err => {
            this.setState({ registerAlert: true });
        });
    }

    login() {
        axios.post('https://fithubserver.herokuapp.com/api/user/login', {
            email: this.state.email,
            password: this.state.password
        }).then(response => {
            this.setState({ token: response.data.token });
        });
    }

    render() {
        let registerAlert = null;
        if (this.state.registerAlert) {
            registerAlert = <Alert color="danger">
                <strong>Oh snap!</strong> Something went wrong :(
                    </Alert>;
        }
        return (
            <Container>
                <Row>
                    <Col>
                        {registerAlert}
                        <Jumbo />
                        <Form>
                            <FormGroup>
                                <Label>Name</Label>
                                <Input type="text" value={this.state.firstName} onChange={this.handleFirstNameChange} placeholder="John" />
                            </FormGroup>
                            <FormGroup>
                                <Label>Last Name</Label>
                                <Input type="text" value={this.state.lastName} onChange={this.handleLastNameChange} placeholder="Doe" />
                            </FormGroup>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input type="email" value={this.state.email} onChange={this.handleEmailChange} placeholder="email@example.com" />
                            </FormGroup>
                            <FormGroup>
                                <Label>Password</Label>
                                <Input type="password" value={this.state.password} onChange={this.handlePasswordChange} placeholder="some password" />
                            </FormGroup>
                            <br />
                            <Button outline onClick={this.handleSubmit} color="success" block > Sign Up </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }
}
